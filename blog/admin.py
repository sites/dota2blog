from django.contrib import admin
from .models import Post, Hero, Hero_Post, Comment

# Register your models here.
admin.site.register(Post)
admin.site.register(Hero)
admin.site.register(Hero_Post)
admin.site.register(Comment)