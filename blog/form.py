from django.forms import ModelChoiceField, ModelForm
from django.core.exceptions import ValidationError
from .models import Post, Hero_Post, Hero

class PostForm(ModelForm):
    main_hero = ModelChoiceField(queryset=Hero.objects.all())
    teammate_hero = ModelChoiceField(queryset=Hero.objects.all(), required=False)
    enemy1 = ModelChoiceField(queryset=Hero.objects.all())
    enemy2 = ModelChoiceField(queryset=Hero.objects.all(), required=False)
    class Meta:
        model = Post
        fields = ['title', 'content', 'lane', 'position', 'dota_version']
    
    def clean(self):
        cleaned_data = super().clean()
        teammate = cleaned_data.get("teammate_hero")
        enemy2 = cleaned_data.get("enemy2")

        if (teammate or enemy2) and not (teammate and enemy2):
            if teammate:
                raise ValidationError({'enemy2': 'Se escolher um amigo, deve escolher um inimigo'})
            if enemy2:
                raise ValidationError({'teammate_hero': 'Se escolher dois inimigos, deve escolher um amigo'})
