# Generated by Django 4.1.1 on 2022-10-31 18:59

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Hero',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=20)),
            ],
        ),
        migrations.CreateModel(
            name='Post',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=100)),
                ('content', models.TextField()),
                ('date', models.DateTimeField(auto_now_add=True)),
                ('lane', models.CharField(choices=[('B', 'Bottom Lane'), ('T', 'Top Lane'), ('M', 'Mid Lane')], default='M', max_length=1)),
                ('position', models.CharField(choices=[('1', 'Mid Lane'), ('2', 'Hard Carrier'), ('3', 'Offlaner'), ('4', 'Suport 4'), ('5', 'Suport 5')], default='1', max_length=1)),
                ('dota_version', models.CharField(max_length=7)),
            ],
        ),
        migrations.CreateModel(
            name='Hero_Post',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('heroe_category', models.CharField(choices=[('1', 'Main Hero'), ('2', 'Teammate Hero'), ('3', 'Enemy Hero')], default='1', max_length=1)),
                ('heroe', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='hero_post', to='blog.hero')),
                ('post', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='hero_post', to='blog.post')),
            ],
        ),
    ]
