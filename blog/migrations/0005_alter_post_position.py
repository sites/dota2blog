# Generated by Django 4.1.1 on 2022-11-01 00:09

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0004_rename_heroe_hero_post_hero'),
    ]

    operations = [
        migrations.AlterField(
            model_name='post',
            name='position',
            field=models.CharField(choices=[('1', 'Midlaner'), ('2', 'Hard Carrier'), ('3', 'Offlaner'), ('4', 'Suport 4'), ('5', 'Suport 5')], default='1', max_length=1),
        ),
    ]
