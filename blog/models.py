from django.conf import settings
from django.db import models
from django.contrib.auth import get_user_model

# Create your models here.

## O modelo Hero é minha classe de categoria, utilizei esse nome 
## para facilitar na hora de criar o programa
class Hero(models.Model):
    name = models.CharField(max_length=20)
    relative_url = models.CharField(max_length=220)

    def __str__(self):
        return self.name

class Post(models.Model):
    title = models.CharField(max_length=100)
    content = models.TextField()
    date = models.DateTimeField(auto_now_add=True)

    LANE_CHOICES = [
        ('B', 'Bottom Lane'),
        ('T', 'Top Lane'),
        ('M', 'Mid Lane'),
    ]
    lane = models.CharField(
        max_length=1,
        choices=LANE_CHOICES
        )
    
    POSITION_CHOICES=[
        ('1', 'Midlaner'),
        ('2', 'Hard Carrier'),
        ('3', 'Offlaner'),
        ('4', 'Suport 4'),
        ('5', 'Suport 5')
    ]
    position = models.CharField(
        max_length=1,
        choices=POSITION_CHOICES,
    )

    dota_version = models.CharField(max_length=7, default='7.31c')

    heroes = models.ManyToManyField(Hero, related_name='posts', through = "Hero_Post")

    def __str__(self):
        return self.title

class Hero_Post(models.Model):
    post = models.ForeignKey(Post, on_delete=models.CASCADE, related_name='hero_post')
    hero = models.ForeignKey(Hero, on_delete=models.CASCADE, related_name='hero_post')


    CATEGORY_CHOICES = [
        ('1', 'Main Hero'),
        ('2', 'Teammate Hero'),
        ('3', 'Enemy Hero'),
    ]
    hero_category = models.CharField(
        max_length=1,
        choices=CATEGORY_CHOICES,
    )

    def __str__(self):
        return self.post.title + ' - ' + self.hero.name + ' - ' + self.hero_category

class Comment(models.Model):
    user = models.ForeignKey(get_user_model(), on_delete=models.CASCADE)
    post = models.ForeignKey(Post, on_delete=models.CASCADE, related_name='comments')
    content = models.CharField(max_length=500)
    date = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return "comentário de " + self.user.username + " no post " + self.post.title