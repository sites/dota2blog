from django.urls import path

from . import views

app_name = 'blog'
urlpatterns = [
    path('', views.PostListView.as_view(), name='list_posts'),
    path('<int:pk>/', views.PostDetailView.as_view(), name='detail'),
    path('update/<int:pk>', views.PostUpdateView.as_view(), name='update'),
    path('delete/<int:pk>', views.PostDeleteView.as_view(), name='delete'),
    path('new_post/', views.PostCreateView.as_view(), name='create'),
    path('comment/<int:post_id>/', views.CommentCreateView.as_view(), name='comment'),
    path('heroes', views.HeroListView.as_view(), name='list_heroes'),
    path('posts/<int:pk>/', views.PostHeroFilter.as_view(), name='filtered_post')
]