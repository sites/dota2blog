import markdown
from markdownify import markdownify
from django.views import generic
from django.shortcuts import render, get_object_or_404, HttpResponseRedirect
from django.urls import reverse
from .models import Post, Hero, Hero_Post, Comment
from django.contrib.auth import get_user_model
from .form import PostForm

# Create your views here.
class PostListView(generic.ListView):
    model = Post
    template_name = 'blog/home.html'

class PostDetailView(generic.DetailView):
    model = Post
    template_name = 'blog/detail.html'
    
class PostCreateView(generic.CreateView):
    model = Post
    form_class = PostForm
    success_url = '/{id}/'
    template_name = 'blog/create.html'
 
    def form_valid(self, form):
        new_post = form.save(commit=False)
        new_post.content = markdown.markdown(form.cleaned_data['content'])
        new_post.save()
        form.save_m2m()

        main_hero = form.cleaned_data['main_hero']
        enemy1 = form.cleaned_data['enemy1']
        teammate = form.cleaned_data['teammate_hero']
        enemy2 = form.cleaned_data['enemy2']

        new_post.heroes.add(main_hero, through_defaults={'hero_category': '1'})
        new_post.heroes.add(enemy1, through_defaults={'hero_category': '3'})
        if enemy2:
            new_post.heroes.add(enemy2, through_defaults={'hero_category': '3'})
            new_post.heroes.add(teammate, through_defaults={'hero_category': '2'})

        return HttpResponseRedirect(
            reverse('blog:detail', args=(new_post.id, )))

class PostUpdateView(generic.UpdateView):
    model = Post
    form_class = PostForm
    success_url = '/{id}/'
    template_name = 'blog/update.html'

    def get_initial(self):
        initial = super().get_initial()
        initial['main_hero'] = super().get_object().hero_post.get(hero_category=1).hero
        initial['enemy1'] = super().get_object().hero_post.filter(hero_category=3)[0].hero
        initial['content'] = markdownify(super().get_object().content)
        if super().get_object().hero_post.filter(hero_category=2).exists():
            initial['teammate_hero'] = super().get_object().hero_post.get(hero_category=2).hero
            initial['enemy2'] = super().get_object().hero_post.filter(hero_category=3)[1].hero
        return initial

    def form_valid(self, form):
        new_post = form.save(commit=False)
        new_post.content = markdown.markdown(form.cleaned_data['content'])
        new_post.save()
        form.save_m2m()

        main_hero = form.cleaned_data['main_hero']
        enemy1 = form.cleaned_data['enemy1']
        teammate = form.cleaned_data['teammate_hero']
        enemy2 = form.cleaned_data['enemy2']

        new_post.heroes.add(main_hero, through_defaults={'hero_category': '1'})
        new_post.heroes.add(enemy1, through_defaults={'hero_category': '3'})
        if enemy2:
            new_post.heroes.add(enemy2, through_defaults={'hero_category': '3'})
            new_post.heroes.add(teammate, through_defaults={'hero_category': '2'})

        return HttpResponseRedirect(
            reverse('blog:detail', args=(new_post.id, )))

class PostDeleteView(generic.DeleteView):
    model = Post
    template_name = 'blog/delete.html'
    context_object_name = 'post'
    success_url = '/'

class CommentCreateView(generic.CreateView):
    model = Comment
    fields = ['content']
    template_name = 'blog/create_comment.html'

    def dispatch(self, request, post_id):
        self.initial = {
            'post': Post.objects.get(pk=post_id),
            'user': get_user_model().objects.all()[1]
        }
        return super().dispatch(request, post_id)

    def form_valid(self, form):
        new_comment = form.save(commit=False)
        new_comment.post = self.initial['post']
        new_comment.user = self.initial['user']
        new_comment.save()
        form.save_m2m()

        return HttpResponseRedirect(
            reverse('blog:detail', args=(new_comment.post.id, )))

class HeroListView(generic.ListView):
    model = Hero
    template_name = 'blog/list_heroes.html'
    context_object_name = 'heroes'

class PostHeroFilter(generic.DetailView):
    model = Hero
    template_name = 'blog/list_filtered_posts.html'
    context_object_name = 'hero'