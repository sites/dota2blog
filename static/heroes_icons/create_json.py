import os
import json

database = []
for file_name in os.listdir('C:/Users/andre/Desktop/Projects/Django/dota2blog/static/heroes_icons'):
    if file_name[-4:]=='.png':
        hero_name = file_name[:-4]
        file_rurl = 'heroes_icons/' + file_name
        fields = {
            'name': hero_name ,
            'relative_url': file_rurl
        }
        data = {
            'model': "blog.hero",
            'fields': fields,
        }
        database.append(data)

# convert into JSON:
json_data = json.dumps(database)

with open("C:/Users/andre/Desktop/Projects/Django/dota2blog/heroes_data.json", "w") as out:
    out.write(json_data)